﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            // 1. Вывод информации о заданном аккаунте по логину и паролю;
            atmManager.GetUserInformation("snow", "111");

            //2. Вывод данных о всех счетах заданного пользователя;
            atmManager.GetBillsInformation("snow", "111");

            //3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;
            atmManager.GetUserBillsHistory("snow", "111");

            //4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта;
            atmManager.GetUserBillsHistory();

            //5. Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой);
            atmManager.GetRichUsers(50);

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}