﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }
        
        // 1. Вывод информации о заданном аккаунте по логину и паролю;
        public  void GetUserInformation(string login, string password)
        {
            int userId = (from u in Users
                         where u.Login == login && u.Password ==password
                         select u.Id).First();
            IEnumerable<User> user_information = Users.Where(x => x.Id == userId);
            foreach (var s in user_information)
            {
                System.Console.WriteLine(s.SurName + " " + s.FirstName + " " + s.MiddleName);
                System.Console.WriteLine("Телефон: " + s.Phone);
                System.Console.WriteLine("Паспортные данные (серия, номер) " + s.PassportSeriesAndNumber);
            }

        }
        //2. Вывод данных о всех счетах заданного пользователя;

        public void GetBillsInformation(string login, string password)
        {
            int userId = (from u in Users
                          where u.Login == login && u.Password == password
                          select u.Id).First();
            IEnumerable<Account> acc_information = Accounts.Where(x => x.UserId == userId);
            foreach (var s in acc_information)
                System.Console.WriteLine("Дата открытия счета" + s.OpeningDate + "; общая сумма " + s.CashAll);

        }
        //3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;
        public void GetUserBillsHistory(string login, string password)
        {
            int userId = (from u in Users
                          where u.Login == login && u.Password == password
                          select u.Id).First();
            var acc_bills_information = from a in Accounts
                                        join o in History on a.Id equals o.AccountId
                                        where a.UserId == userId
                                        select new { OpeningDate = a.OpeningDate, CashAll = a.CashAll, OperationDate = o.OperationDate, OperationType = o.OperationType, CashSum = o.CashSum};

            foreach (var s in acc_bills_information)
                System.Console.WriteLine(s);

        }

        //4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта;
        public void GetUserBillsHistory()
        {
            var acc_bills_information = from a in Accounts
                                        join o in History on a.Id equals o.AccountId
                                        join u in Users on a.UserId equals u.Id
                                        where  o.OperationType == OperationType.InputCash
                                        select new {UserName = u.SurName + " " + u.FirstName, OperationDate = o.OperationDate, CashSum = o.CashSum };

            foreach (var s in acc_bills_information)
                System.Console.WriteLine(s);

        }

        //5. Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой);
        public void GetRichUsers(decimal cash)
        {
            var user_rich_information = (from a in Accounts
                                        join u in Users on a.UserId equals u.Id
                                        where a.CashAll > cash
                                        select new { UserName = u.SurName + " " + u.FirstName, PassportData = u.PassportSeriesAndNumber, Phone = u.Phone }).Distinct();

            System.Console.WriteLine("У этих клиентов есть деньги! Кредиты не предлагать!");
            System.Console.WriteLine("");
            System.Console.WriteLine("---------------------------------------------------");
            foreach (var s in user_rich_information)
            {
                System.Console.WriteLine(s);
                System.Console.WriteLine("---------------------------------------------------");
            }

         }
    }
}